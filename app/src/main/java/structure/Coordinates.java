package main.java.structure;

import java.util.Set;

public class Coordinates {
    
    private final int x;
    private final int y;

    public Coordinates(final int x, final int y) {
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return this.x;
    }

    public int getY() {
        return this.y;
    }

    public Set<Coordinates> getAdjacentCoordinates() {
        return Set.of(new Coordinates(this.x - 1, this.y),
                      new Coordinates(this.x + 1, this.y),
                      new Coordinates(this.x, this.y - 1),
                      new Coordinates(this.x, this.y + 1));
    }

    @Override
    public String toString() {
        return "[" + x + ", " + y + "]";
    }

    @Override
    public boolean equals(Object coord) {
        if (!(coord instanceof Coordinates)) {
            return false;
        } else {
            return this.x == ((Coordinates) coord).x && this.y == ((Coordinates) coord).y;
        }
    }
    
}
