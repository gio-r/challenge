package main.java.structure;

public class PlacedAntenna {
    private final Antenna antenna;
    private final Coordinates coordinates;

    public PlacedAntenna(final Antenna antenna, final Coordinates coordinates) {
        this.antenna = antenna;
        this.coordinates = coordinates;
    }

    public Antenna getAntenna() {
        return this.antenna;
    }

    public Coordinates getCoordinates() {
        return this.coordinates;
    }
    
}
