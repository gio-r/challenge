package main.java.structure;

public class Antenna {
    private final int id;
    private final int range;
    private final int connectionSpeed;

    public Antenna(final int id, final int range, final int speed){
        this.id = id;
        this.range = range;
        this.connectionSpeed = speed;
    }

    public int getId(){
        return this.id;
    }

    public int getRange(){
        return this.range;
    }

    public int getConnectionSpeed(){
        return this.connectionSpeed;
    }

    @Override       
    public String toString(){
        return("ID: "+ this.getId() + " range: " + this.getRange() + " speed: " + this.getConnectionSpeed());
    }

    @Override
    public boolean equals(Object ant2){
        if (ant2 instanceof Antenna) return (((Antenna) ant2).getId() == this.getId());
        else return false;
    }
    
}
