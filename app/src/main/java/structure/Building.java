package main.java.structure;

public class Building {
    private final Coordinates coordinates;
    private final int latencyWeight;
    private final int speedWeight; 
    private final int id;

    public Building(final int id, final Coordinates coordinates, final int latencyWeight, final int speedWeight) {
        this.id = id;
        this.coordinates = coordinates;
        this.latencyWeight = latencyWeight;
        this.speedWeight = speedWeight;
    }

    public int getId() {
        return this.id;
    }

    public Coordinates getCoordinates() {
        return this.coordinates;
    }

    public int getLatencyWeight() {
        return this.latencyWeight;
    }

    public int getSpeedWeight() {
        return this.speedWeight;
    }

    @Override
    public String toString(){
        return ("ID: "+ this.getId() + " coordinates: " + this.getCoordinates() + " lat weigth: " + this.latencyWeight + " speed weight: " + this.speedWeight);
    }

    @Override
    public boolean equals(Object o) {
        return (o instanceof Building) && (((Building) o).getId() == this.getId());
    }
    
}