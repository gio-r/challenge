package main.java.io;

import java.io.PrintWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Map;
import java.util.Set;

import main.java.structure.Coordinates;
import main.java.structure.PlacedAntenna;

public class OutputParser {

    public OutputParser() {}

    public void printOutput(final String name, final Set<PlacedAntenna> result) {
        try (PrintWriter writer = new PrintWriter(new FileWriter("src/main/resources/outputs/" + name + ".out"))) {
            writer.println(result.size());
            result.stream()
                  .sorted((a1, a2) -> a1.getAntenna().getId() - a2.getAntenna().getId())
                  .forEach(entry -> {
                    writer.println(entry.getAntenna().getId() + " " + entry.getCoordinates().getX() + " " + entry.getCoordinates().getY());
            });
          } catch (IOException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
          }
    }
    
}
