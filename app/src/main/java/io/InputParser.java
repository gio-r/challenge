package main.java.io;

import java.io.File;
import java.nio.file.Files;
import java.util.List;
import java.util.LinkedList;
import java.util.Set;
import java.util.HashSet;

import main.java.structure.Building;
import main.java.structure.Antenna;
import main.java.structure.Coordinates;

public class InputParser {
	
	private int gridWidth;
	private int gridHeight;
	
	private List<String> buildingLines = new LinkedList<>();
	private List<String> antennaLines = new LinkedList<>();
	private int reward;

    public InputParser() {}

	public void loadInput(String inputName) throws Exception {
        final List<String> input = Files.readAllLines(new File("src/main/resources/inputs/" + inputName + ".in")
                                        .toPath());
		this.gridWidth = Integer.parseInt(input.get(0).split(" ")[0]);
		this.gridHeight = Integer.parseInt(input.get(0).split(" ")[1]);
		
		int nBuildings = Integer.parseInt(input.get(1).split(" ")[0]);
		this.buildingLines = input.subList(2, 2 + nBuildings);
		
		int nAntennas = Integer.parseInt(input.get(1).split(" ")[1]);
		this.antennaLines = input.subList(2 + nBuildings, 2 + nBuildings + nAntennas);

		this.reward = Integer.parseInt(input.get(1).split(" ")[2]);
	}

	public int parseGridWidth() {
		return this.gridWidth;
	}

	public int parseGridHeight() {
		return this.gridHeight;
	}

	public int getReward() {
		return this.reward;
	}

	public Set<Building> parseBuildings() {
		final Set<Building> buildings = new HashSet<>();
		int id = 0;
		for (String buildingLine : this.buildingLines) {
			buildings.add(this.parseBuilding(buildingLine, id));
			id++;
		}
		return buildings;
	}
	
	private Building parseBuilding(String buildingLine, int id) {
		String[] buildingData = buildingLine.split(" ");
		Coordinates buildingCoord = new Coordinates(Integer.parseInt(buildingData[0]), Integer.parseInt(buildingData[1]));
		int latencyWeight = Integer.parseInt(buildingData[2]);
		int connecitonSpeedWeight = Integer.parseInt(buildingData[3]);

		return (new Building(id, buildingCoord, latencyWeight, connecitonSpeedWeight));
	}

	private Antenna parseAntenna(String antennaLine, int id) {
		String[] antennaData = antennaLine.split(" ");
		int range = Integer.parseInt(antennaData[0]);
		int connectionSpeed = Integer.parseInt(antennaData[1]);

		return (new Antenna(id, range, connectionSpeed));
	}

	public Set<Antenna> parseAntennas() {
        final Set<Antenna> antennas = new HashSet<>();
        int id = 0;
		for (String antennaLine : this.antennaLines) {
			antennas.add(this.parseAntenna(antennaLine, id));
			id++;
		}
		return antennas;
	}
}

