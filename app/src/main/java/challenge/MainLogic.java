package main.java.challenge;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import main.java.structure.Antenna;
import main.java.structure.Building;
import main.java.structure.Coordinates;
import main.java.structure.PlacedAntenna;

import org.apache.commons.lang3.tuple.Pair;

public class MainLogic {
    private final Set<Antenna> antennas;
    private final Set<Building> buildings;
    private final int gridWidth;
    private final int gridHeight;
    private final int reward;

	private List<Building> orderBuildings(Set<Building> buildingsSet) {
		List<Building> sortedBuildings = new LinkedList<>();
		sortedBuildings.addAll(buildingsSet);
		sortedBuildings.sort((b1, b2) -> Integer.compare(b1.getSpeedWeight()*1 + b1.getLatencyWeight()*0, b2.getSpeedWeight()*1 + b2.getLatencyWeight()*0));
		return sortedBuildings;
    }

	private List<Antenna> orderAntennas(Set<Antenna> antennasSet) {
		List<Antenna> sortedAntennas = new LinkedList<>();
		sortedAntennas.addAll(antennasSet);
		sortedAntennas.sort((a1, a2) -> Integer.compare(a1.getConnectionSpeed(), a2.getConnectionSpeed()));
		return sortedAntennas;
	}

    public MainLogic(int width, int height, int reward, Set<Antenna> antennas, Set<Building> buildings) {
        this.gridHeight = height;
        this.gridWidth = width;
        this.reward = reward;
        this.antennas = antennas;
        this.buildings = buildings;
    }

    public Set<PlacedAntenna> resolve() {  
        Set<PlacedAntenna> placedAntennas = new HashSet<>();
        Set<Antenna> antennasToBePlaced = new HashSet<>(this.antennas);
        Set<Building> buildingsToBeCovered = new HashSet<>(this.buildings);
        Set<Coordinates> coverablePlaces = new HashSet<>();
        //this.totalScore = 0;
        for (int i = 0; i < this.gridWidth; i++) {
            for (int j = 0; j < this.gridHeight; j++) {
                coverablePlaces.add(new Coordinates(i, j));
            }
        }	
        do {
            Pair<PlacedAntenna, Set<Building>> a = this.placeBestAntennaToCoverBuildings(antennasToBePlaced, buildingsToBeCovered, coverablePlaces);
            placedAntennas.add(a.getLeft());
            antennasToBePlaced.remove(a.getLeft().getAntenna());
            // buildingsToBeCovered.removeAll(this.buildingsCovered(a));
            buildingsToBeCovered = a.getRight();
            coverablePlaces.remove(a.getLeft().getCoordinates());
        } while (!antennasToBePlaced.isEmpty() && !buildingsToBeCovered.isEmpty() && !coverablePlaces.isEmpty());
        // for (final Antenna antenna: antennasToBePlaced) {
        //     if (!buildingsToBeCovered.isEmpty() && !coverablePlaces.isEmpty()) {
        //         PlacedAntenna a = this.placeAntennaInBestPlaceToCoverBuildings(antenna, buildingsToBeCovered, coverablePlaces);
        //         placedAntennas.add(a);
        //         buildingsToBeCovered.removeAll(this.buildingsCovered(a));
        //         coverablePlaces.remove(a.getCoordinates());
        //     }
        // }
        return placedAntennas;
    }
    
    private int getDistance(Coordinates c1, Coordinates c2){
        return (Math.abs(c1.getX()-c2.getX()) + Math.abs(c1.getY()-c2.getY()));
    }

	private int getSingleScore(PlacedAntenna a, Building b){
        if (this.getDistance(a.getCoordinates(), b.getCoordinates()) <= a.getAntenna().getRange()) {
            return (b.getSpeedWeight()*a.getAntenna().getConnectionSpeed() - b.getLatencyWeight()*getDistance(a.getCoordinates(), b.getCoordinates()));
		} else {
			return -1;
		}
    }

    private Set<Building> buildingsCovered(final PlacedAntenna antenna) {
        Set<Building> bC = new HashSet<>();
        for(Building b: buildings) {
            if(getDistance(b.getCoordinates(), antenna.getCoordinates()) <= antenna.getAntenna().getRange()) {
                bC.add(b);
			}
        }
        return bC;
    }

    private Pair<PlacedAntenna, Set<Building>> placeBestAntennaToCoverBuildings(final Set<Antenna> antennas, final Set<Building> buildings, final Set<Coordinates> grid) {
        PlacedAntenna placedAntenna = null;
		Set<Building> toBeCoveredBuildings = null;
		int maxScore = 0;
		for (Antenna a : antennas) {
			for(Coordinates c : grid) {
				PlacedAntenna pa = new PlacedAntenna(a, c);
				Set<Building> toBeCoveredTmp = new HashSet<>(buildings);
				int positionScore = 0;
				for(Building b : buildings) {
					int singleScore = this.getSingleScore(pa, b);
					if (singleScore != -1) {
						positionScore += singleScore;
						toBeCoveredTmp.remove(b);
					}
				}
				if (positionScore > maxScore) {
					placedAntenna = pa;
					maxScore = positionScore;
					toBeCoveredBuildings = toBeCoveredTmp;
				}
			}
		}
		return Pair.of(placedAntenna, toBeCoveredBuildings);
    }

    private PlacedAntenna placeAntennaInBestPlaceToCoverBuildings(final Antenna antenna, final Set<Building> buildings, final Set<Coordinates> coordinates) {
        PlacedAntenna placedAntenna = null;
		int maxScore = 0;
		for(Coordinates c : coordinates) {
			PlacedAntenna pa = new PlacedAntenna(antenna, c);
			int positionScore = 0;
			for(Building b : buildings) {
				positionScore += this.getSingleScore(pa, b);
			}
			if (positionScore > maxScore) {
				placedAntenna = pa;
				maxScore = positionScore;
			}
		}
		return placedAntenna;
    }

     private PlacedAntenna placeBestAntennaToCoverBuildings2(final Set<PlacedAntenna> pA, final Set<Antenna> antennas, final Set<Building> buildings, final Set<Coordinates> grid) {
        PlacedAntenna placedAntenna = null;
		Set<PlacedAntenna> tempPA = new HashSet<>(pA);
        int maxScore = 0;
		for (Antenna a : antennas) {
            for(Coordinates c : grid) {
                PlacedAntenna pa = new PlacedAntenna(a, c);
				tempPA.add(pa);
                int positionScore = getTotalScore(tempPA, buildings);
				tempPA.remove(pa);
                if (positionScore > maxScore) {
					placedAntenna = pa;
					maxScore = positionScore;
				}
			}
		}
		return placedAntenna;
    }

	public int getTotalScore(final Set<PlacedAntenna> antennas, final Set<Building> buildings) {
		int totalScore = 0;
		boolean allBuildingsCovered = true;
		for (Building b : buildings) {
			int maxBuildingScore = 0;
			for (PlacedAntenna a : antennas) {
				int currentScore = this.getSingleScore(a, b);
				if(currentScore > maxBuildingScore) {
					maxBuildingScore = currentScore;
				}
			}
			totalScore += maxBuildingScore;
			allBuildingsCovered = (maxBuildingScore == 0) ? false : allBuildingsCovered;
		}
		if (allBuildingsCovered) {
			totalScore += this.reward;
		}
		return totalScore;
    }

/*

a1: r:2 s:100
a2: r:2 s:49

b1: lw:1 sw:32
b2: lw:4 sw:44

s(a1b1) = 32*100 - 1*dist
s(a1b2) = 32*49 - 1*dist
s(a2b1) = 44*100 - 4*dist
s(a2v2) = 44*49 - 4*dist

*/


}
